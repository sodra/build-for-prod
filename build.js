#!/usr/bin/env node

const fs = require('fs')
const path = require('path')
const md5 = require('md5')

const inDir = process.argv[2] || 'src'
const outDir = process.argv[3] || 'dist'

const findImages = (file) => {
  let newFile = file.slice()
  let regex = RegExp('(\\./.+?\\.(png|jpg|svg|ico))', 'g')
  let match
  while ((match = regex.exec(file)) !== null) {
    const extname = path.extname(match[1])
    const basename = path.basename(match[1], extname)
    const data = fs.readFileSync(path.join(inDir, match[1]))
    const hash = md5(data).slice(0, 8)
    const name = `${basename}-${hash}${extname}`
    fs.writeFileSync(path.join(outDir, name), data)
    newFile = newFile.replace(match[1], '/' + name)
  }
  return newFile
}

const findCSS = (file) => {
  let newFile = file.slice()
  const regex = RegExp('(\\./.+?\\.css)', 'g')
  let match
  while ((match = regex.exec(file)) !== null) {
    const extname = path.extname(match[1])
    const basename = path.basename(match[1], extname)
    let data = fs.readFileSync(path.join(inDir, match[1]), 'utf8')
    data = findImages(data)
    const hash = md5(data).slice(0, 8)
    const name = `${basename}-${hash}${extname}`
    fs.writeFileSync(path.join(outDir, name), data.replace(/\s+/g, ' '))
    newFile = newFile.replace(match[1], '/' + name)
  }
  return newFile
}

const files = fs.readdirSync(inDir).filter((file) => file.endsWith('.html'))
for (let file of files) {
  let html = fs.readFileSync(path.join(inDir, file), 'utf8')
  html = findImages(html)
  html = findCSS(html)
  fs.writeFileSync(path.join(outDir, file), html)
}
